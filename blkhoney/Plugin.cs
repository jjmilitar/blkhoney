﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;

namespace blkhoney
{
    public class Plugin : IPlugin
    {
        #region Secure/Unsecure Configuration Setup
        private string _secureConfig = null;
        private string _unsecureConfig = null;

        public Plugin(string unsecureConfig, string secureConfig)
        {
            _secureConfig = secureConfig;
            _unsecureConfig = unsecureConfig;
        }
        #endregion
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            try
            {
                Entity entity = (Entity)context.InputParameters["Target"];
                DateTime startDate = (DateTime)entity.Attributes["a1a_startdate"];
                DateTime endDate = (DateTime)entity.Attributes["a1a_enddate"];

                DateTime today = DateTime.Now;
                OptionSetValue typeOfLeave = (OptionSetValue)entity.Attributes["a1a_typeofleave"];
                if (typeOfLeave.Value == 548070000)
                {
                    if (DateTime.Compare(startDate,today.AddDays(1)) > 0)
                    {
                        throw new InvalidPluginExecutionException("Error");
                    }
                }

                if(DateTime.Compare(startDate,today.AddDays(-1)) < 0)
                {
                    throw new InvalidPluginExecutionException("Error");
                }

                List<DateTime> dates = Enumerable.
                    Range(1, (endDate - startDate).Days + 1).Select(d => startDate.AddDays(d))
                    .Where(d=> d.DayOfWeek != DayOfWeek.Saturday && d.DayOfWeek != DayOfWeek.Sunday)
                    .ToList();
                entity.Attributes["a1a_numberofdays2"] = dates.Count;
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException(e.Message);
            }
        }
    }
}
